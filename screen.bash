#!/bin/bash
#ixrandr --output HDMI1 --mode 1920x1080

usageMessage="Usage: screen [l=left|r=right|d=disconnect]"

if [[ $# -ne 1 ]]
then
	echo $usageMessage
	exit 1;
fi;

if [[ $1 = l ]] 
then
	xrandr --output HDMI1 --mode 1920x1080 --auto --right-of eDP1
	xrandr --output HDMI1 --brightness 0.7
elif [[ $1 = m ]]
then
	xrandr --output HDMI1 --mode 1920x1080 --auto
	xrandr --output HDMI1 --brightness 0.7
elif [[ $1 = r ]]
then
	xrandr --output HDMI1 --mode 1920x1080 --auto --left-of eDP1
	xrandr --output HDMI1 --brightness 0.7
elif [[ $1 = d ]]
then
	xrandr --output HDMI1 --off
else
	echo $usageMessage
	exit 2
fi;

exit 0

